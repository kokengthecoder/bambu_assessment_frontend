### DEMO website
[http://d19v91pr27nwp8.cloudfront.net/](http://d19v91pr27nwp8.cloudfront.net/)

### Development
```
$ yarn
$ yarn start
```

### Build
```
$ yarn build
```

### Things to be improved
- Options for candlestick interval
- Realtime data
