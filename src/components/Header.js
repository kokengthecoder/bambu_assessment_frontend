import React from 'react'

const Header = ({ child }) => {
	return (
		<div id="header">{child}</div>
	)
}

export default Header