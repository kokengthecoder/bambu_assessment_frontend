// import library
import React from 'react'
import MenuItem from './MenuItem'
import _ from 'lodash'

const Menu = ({ selected, titles, onMenuSelect }) => {
	if (titles.length < 1) return (<div></div>)

	const renderList = _.map(titles, (t) => 
		<MenuItem 
			key={t}
			title={t}
			selected={selected}
			onMenuSelect={onMenuSelect} />)

	return (
		<div>
			{ renderList }
		</div>
	)
}

export default Menu