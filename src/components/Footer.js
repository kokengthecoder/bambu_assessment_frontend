import React from 'react'

const Footer = ({ child }) => {
	return (
		<div id="footer">{child}</div>
	)
}

export default Footer