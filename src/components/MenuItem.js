// import library
import React from 'react'

// import css
import './MenuItem.css'

const MenuItem = ({ selected, title, onMenuSelect }) => {
	const className = `menu-item ${selected === title ? 'selected' : ''}`

	return (
		<div 
			className={className}
			onClick={() => onMenuSelect(title)}>
			{title}
		</div>
	)
}

export default MenuItem