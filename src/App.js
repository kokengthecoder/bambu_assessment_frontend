// import library
import React, { Component } from 'react'
import { connect } from 'react-redux'
import modAlphaVantageData from './lib/mod_alpha_vantage_data'
import axios from 'axios'

// import Containers
import Chart from './containers/Chart'

// import Components
import Menu from './components/Menu'
import Banner from './components/Banner'

// import CSS
import './App.css'

// import action
import { loadStock } from './store/actions/chart.action'

class App extends Component {
	constructor() {
		super()

		this.stockLists = ['MSFT', 'AAPL', 'INTC', 'NFLX', 'ORCL', 'CMCSA', 'GOOG', 'LUV', 'HOG', 'GOOGL', 'AMZN']
	
		this.onMenuSelect = this.onMenuSelect.bind(this)
	}

	async componentDidMount() {
		this.props.loadStock('MSFT')
	}

	onMenuSelect(stock) {
		this.props.loadStock(stock)
	}

  render() {
  	const {
  		stockLists,
  		props: { chartData },
  		onMenuSelect
  	} = this

    return (
    	<div id="app-container">
    		<div className="row banner" id="header">
    			<Banner>
    				A Header
    			</Banner>
    		</div>
    		<div id="chart-container">
    			<div id="chart-menu">
		    		<Menu 
		    			titles={stockLists}
		    			selected={chartData.selected}
		    			onMenuSelect={onMenuSelect} />
    			</div>
		      <div id="chart">
			      <Chart chartData={chartData} />
		      </div>
    		</div>
    		<div className="row banner" id="footer">
    			<Banner>
    				a footer
    			</Banner>
    		</div>
    	</div>
    )
  }
}

const mapDispatchToProps = dispatch => {
	return { loadStock: stock => dispatch(loadStock(stock))}
}

const mapStateToProps = state => {
	return { chartData: state.chart }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)