export const LOAD_STOCK = 'LOAD_STOCK'

const initialState = {
	selected: '',
	metaData: {},
	data: [],
	minX: 1,
	maxX: 1,
	minY: 1,
	maxY: 1
}

export default function (state = initialState, action) {
	const { payload, type } = action

	switch(type) {
		case LOAD_STOCK:
			return {
				...state,
				...payload
			}
		default:
			return state
	}
}