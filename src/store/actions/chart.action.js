// import library
import axios from 'axios'
import modAlphaVantageData from '../../lib/mod_alpha_vantage_data'

import { LOAD_STOCK } from '../reducers/chart.reducer'

const config = {
	apikey: '45MPXD31WRDBQPZ7',
	interval: '60min',
	function: 'TIME_SERIES_INTRADAY'
}

export const loadStock = (stock) => {
	return async dispatch => {

		const { data } = await axios({
			method: 'get',
			url: 'https://www.alphavantage.co/query',
			params: {
				...config,
				symbol: stock
			}
		})

		return dispatch({
			type: LOAD_STOCK,
			payload: { 
				...modAlphaVantageData(data),
				selected: stock
			}
		})
	}
}