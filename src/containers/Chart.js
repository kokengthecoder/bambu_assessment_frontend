// import library
import _ from 'lodash'
import React, { Component } from 'react'
import generateChartLabels from '../lib/generate_chart_labels'

// import css
import './Chart.css'

class Chart extends Component {

	getChartSVGMinMax() {
		return {
		 chartMinX: this.props.svgWidth * this.props.labelWidth,
		 chartMaxX: this.props.svgWidth,
		 chartMinY: 0,
		 chartMaxY: this.props.svgHeight * (1 - this.props.labelWidth)
		}
	}

	getChartLabelMinMax() {
		const { chartData: { minY, maxY } } = this.props

		return {
			labelMinY: +(Math.floor(minY)),
			labelMaxY: +(Math.round(maxY))
		}
	}

	getSvgX (x) {
		const { chartData: { minX, maxX } } = this.props
		const { chartMinX, chartMaxX } = this.getChartSVGMinMax()
		const { labelMinY, labelMaxY } = this.getChartLabelMinMax()

		const rangeX = maxX - minX
		const rangeChartX = chartMaxX - chartMinX

		return chartMinX + ((x - minX) / rangeX * rangeChartX)
	}

	getSvgY (y) {
		const { chartData: { minY, maxY } } = this.props
		const { chartMinY, chartMaxY } = this.getChartSVGMinMax()
		const { labelMinY, labelMaxY } = this.getChartLabelMinMax()

		const rangeLabelY = labelMaxY - labelMinY
		const rangeChartY = chartMaxY - chartMinY

		return (chartMaxY - ((y - labelMinY) / rangeLabelY * rangeChartY))
	}

	// draw label on Y axis
	generateLabelY () {
		const { nlabels } = this.props
		const { labelMinY, labelMaxY } = this.getChartLabelMinMax()
		const { chartMinY, chartMaxY, chartMinX } = this.getChartSVGMinMax()
		
		const rangeLabelY = labelMaxY - labelMinY
		const rangeChartY = chartMaxY - chartMinY
		
		// calculate interval
		const interval = +(Math.round(rangeLabelY / nlabels * 100) / 100).toPrecision(1)

		// calculate labels positions in svg
		const labels = generateChartLabels(labelMinY, labelMaxY, interval).map(l => {
			const x = chartMinX
			const y = (chartMaxY - ((l - labelMinY) / rangeLabelY * rangeChartY))

			return (
				<text x={x} y={y}
					className="svg_text"
					textAnchor="end">
					 ${l} - 
				</text>
			)
		})

		return (
			<g>
				{labels}
			</g>
		)
	}

  // build graph axis
  drawAxis () {
  	const { chartData: { minX, maxX , minY, maxY } } = this.props
		const { labelMinY, labelMaxY } = this.getChartLabelMinMax()  	

	  return (
	    <g className="chart_axis">
	    	{ this.drawLine(minX, maxX, labelMinY, labelMinY) }
	    	{ this.drawLine(minX, minX, labelMinY, labelMaxY) }
	    </g>
    );
  }

  drawBackgroundGrid () {
  	const dataSize = this.props.chartData.data.length
  	const { labelMinY, labelMaxY } = this.getChartLabelMinMax()

  	const gridX = _.times(dataSize, i => {
  		return this.drawLine(i + 1, i + 1, labelMinY, labelMaxY, this.props.color)
  	})

  	return (
  		<g className="chart_backgroun_grid">
  			{gridX}
  		</g>
  	)
  }

  drawLine (x1, x2, y1, y2, color) {
  	return <line
      x1={this.getSvgX(x1)} y1={this.getSvgY(y1)}
      x2={this.getSvgX(x2)} y2={this.getSvgY(y2)} style={{stroke: color}}/>
  }

  renderCandles () {
  	const { data } = this.props.chartData

		return _.reduce(data, (arr, d, i) => {

			const color = d.close > d.open ? 'green' : 'red'

			const candleHeight = this.drawLine(i + 1, i + 1, d.low, d.high, color)
			
			const openLine = this.drawLine(i + 0.6, i + 1, d.open, d.open, color)

			const closeLine = this.drawLine(i + 1, i + 1.4, d.close, d.close, color)


			return arr.concat([candleHeight, openLine, closeLine])
		}, [])  	
  }

	render() {
		const { svgHeight, svgWidth, chartData } = this.props;

		if (chartData.data.length < 1) {
			return (<div>Loading</div>)
		}

		return (
			<svg viewBox={`0 0 ${svgWidth} ${svgHeight}`}>
        {this.drawAxis()}
        {this.drawBackgroundGrid()}
				{this.generateLabelY()}
				{this.renderCandles()}
      </svg>
		)
	}
}

Chart.defaultProps = {
  color: '#bdc3c7',
  svgHeight: 300,
  svgWidth: 700,
  labelWidth: 0.1, // percentage of svg view,
  nlabels: 10, // roughly how many labels show in axis
}

export default Chart