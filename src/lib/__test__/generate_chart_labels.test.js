import test from 'ava'
import func from '../generate_chart_labels'

test('should generate correct labels', t => {
	const start = 10
	const end = 20
	const interval = 2
	t.deepEqual(func(start, end, interval), [10, 12, 14, 16, 18, 20])
})

test('should generate last number greater than end', t => {
	const start = 10
	const end = 20
	const interval = 3
	t.deepEqual(func(start, end, interval), [10, 13, 16, 19, 22])
})

test('should return only one item if start equal to end', t => {
	t.deepEqual(func(10, 10, 3), [10])
})

test('should return empty array if params missing', t => {
	t.deepEqual(func(10, 20).message, 'params missing')
	t.deepEqual(func(10, undefined, 2).message, 'params missing')
	t.deepEqual(func(undefined, 20, 2).message, 'params missing')
})