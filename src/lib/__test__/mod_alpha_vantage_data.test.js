import test from 'ava'
import func from '../mod_alpha_vantage_data'
import data from './mock_data.json'

test('should test with correct output', t => {
	const result = func(data)

	t.is(result.minX, 1)
	t.is(result.maxX, 5)
	t.is(result.minY, 106.6250)
	t.is(result.maxY, 107.2450)
	t.is(result.data[0].open, 106.6900)
	t.is(result.data[0].high, 107.1800)
	t.is(result.data[0].low, 106.6250)
	t.is(result.data[0].close, 107.1200)
	t.is(result.data[0].volume, 1983448)
})