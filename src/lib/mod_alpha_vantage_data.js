import _ from 'lodash'

export default (apiData) => {
	// if (!apiData) return {}

	const chartLabel = _.find(_.keys(apiData), key => {
		return (new RegExp (/Time\sSeries/g).test(key))
	})

	const chartData = _.values(apiData[chartLabel])

	// assign label
	const openLabel = '1. open'
	const closeLabel = '4. close'
	const lowLabel = '3. low'
	const highLabel = '2. high'
	const volumeLabel = '5. volume'

	const minX = 1
	const maxX = chartData.length

	// init minY and maxY value from the first index
	const { minY, maxY, modData } = _.reduce(chartData, ({ minY, maxY, modData }, d) => {
		return {
			// assign minY if current value is smaller
			minY: minY < d[lowLabel] ? minY : d[lowLabel],
			// assign max if current value is greater
			maxY: maxY > d[highLabel] ? maxY : d[highLabel],

			// convert to float number
			modData: modData.concat([
				{
					open: parseFloat(d[openLabel]),
					close: parseFloat(d[closeLabel]),
					low: parseFloat(d[lowLabel]),
					high: parseFloat(d[highLabel]),
					volume: parseFloat(d[volumeLabel])
				}
			])
		}
	}, { minY: chartData[0][lowLabel], maxY: chartData[0][highLabel], modData: [] })

	return { 
		minX,
		maxX,
		minY: parseFloat(minY),
		maxY: parseFloat(maxY),
		data: _.reverse(modData),
		metadata: apiData['Meta Data']
	}
}
