// initial arr with start
const generateChartLabels = (start, end, interval, arr = [start]) => {
	if (!start || !end || !interval) return (new Error('params missing'))

  if (start === end) return arr

  let lastIterateNumber = (start * 100 + interval * 100) / 100
  
  arr.push(lastIterateNumber)

  // iterate again if smaller than end value
  if (lastIterateNumber < end) {
    generateChartLabels(lastIterateNumber, end, interval, arr)
  }

  return arr
}

export default generateChartLabels